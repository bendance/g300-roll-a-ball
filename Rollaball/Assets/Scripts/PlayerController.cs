﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;
using System.Collections.Specialized;
using System.Diagnostics;

public class PlayerController : MonoBehaviour
{
    public float speed = 0;
    public TextMeshProUGUI countText;
    public TextMeshProUGUI healthText;
    public GameObject winTextObject;
    public GameObject loseTextObject;
    public int forceConst = 50;
    public LayerMask groundLayers;
    public float jumpForce = 7;
    public SphereCollider col;
    public GameObject player;
    public GameObject enemy;

    private Rigidbody rb;
    private int count;
    private int health;
    private float movementX;
    private float movementY;

    // Start is called before the first frame update

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<SphereCollider>();
        count = 0;
        health = 3;

        SetCountText();
        SetHealthText();
        loseTextObject.SetActive(false);
        winTextObject.SetActive(false);
    }

    private void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();

        movementX = movementVector.x;
        movementY = movementVector.y;
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if(count >= 12) 
        {
            enemy.gameObject.SetActive(false);
            winTextObject.SetActive(true);
            Destroy(loseTextObject);
        }
    }

    void SetHealthText()
    {
        healthText.text = "Health: " + health.ToString();
        if (health <= 0)
        {
            player.SetActive(false);
            loseTextObject.SetActive(true);
        }

    }

    private void FixedUpdate()
    {
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);

        rb.AddForce(movement * speed);
    }

    void Update()
    {
        if (IsGrounded() && Input.GetKeyDown(KeyCode.Space))
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        }
    }

    private bool IsGrounded()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x, 
            col.bounds.min.y, col.bounds.center.z), col.radius * .9f, groundLayers);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;

            SetCountText();
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            health = health - 1;

            SetHealthText();
        }

        if (other.gameObject.CompareTag("Fall Plane"))
        {
            player.SetActive(false);
            enemy.SetActive(false);

            if (loseTextObject != null)
            {
                loseTextObject.SetActive(true);
            }
        }
    }
}
